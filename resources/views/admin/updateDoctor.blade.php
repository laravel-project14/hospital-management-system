<!DOCTYPE html>
<html lang="en">

<head>
    <base href="/public" />
    @include('admin.css')

</head>

<body>
    <div class="container-scroller">
        <div class="row p-0 m-0 proBanner" id="proBanner">
            <div class="col-md-12 p-0 m-0">
                <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
                    <div class="ps-lg-1">
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="mb-0 font-weight-medium me-3 buy-now-text">Free 24/7 customer support, updates, and more with this template!</p>
                            <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-between">
                        <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
                        <button id="bannerClose" class="btn border-0 p-0">
                            <i class="mdi mdi-close text-white me-0"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- partial:partials/_sidebar.html -->
        @include('admin.sidebar')
        <!-- partial -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div class="container mt-5" align="center">


                @if(session()->has('message'))
                <div class="alert alert-success">
                <button type="submit" class="close" data-dismiss="alert">
                        x
                    </button>
                {{session()->get('message')}}
                </div>

                @endif
                <form action="{{url('edit_doctor',$doctor->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="wrap">
                        <label for="doctorName" class="form-label">Doctor Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Doctor Name" 
                        id="doctorName" value="{{$doctor->name}}" style="color:black;">
                    </div>
                    <div class="wrap">
                        <label for="phone" class="form-label">Phone Number</label>
                        <input type="text" class="form-control" name="phone" placeholder="Phone number"
                         id="phone" value="{{$doctor->phone}}" style="color:black;">
                    </div>
                    <div class="wrap">
                        <label for="room" class="form-label">Room Number</label>
                        <input type="text" class="form-control" name="room" placeholder="Room number"
                         id="room" value="{{$doctor->room}}" style="color:black;">
                    </div>
                    <div class="wrap">
                        <label for="speciality" class="form-label">Speciality</label>
                        <input type="text" class="form-control" name="speciality" 
                         id="speciality" value="{{$doctor->speciality}}" style="color:black;">
                    </div>
                    <div class="wrap">
                        <label for="image" class="form-label">Old Image</label>
                        <img src="doctorimage/{{$doctor->image}}"  width="100px"
                         id="image" style="color:black;">
                    </div>

                    <div class="wrap mt-3">
                        <input class="form-control form-control-sm" type="file" name="file">
                    </div>
                    <div class="wrap">
                        <button type="submit" class="btn btn-success form-control">Submit</button>
                    </div>

                </form>
            </div>
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('admin.js')
</body>

</html>