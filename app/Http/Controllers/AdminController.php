<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Doctor;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function addView()
    {
        return view('admin.add_doctor');
    }
    public function upload(Request $request)
    {
        $doctor = new Doctor();

        $image = $request->file;
        $imageName = time() . "." . $image->getClientOriginalExtension();
        $request->file->move('doctorimage', $imageName);
        $doctor->image = $imageName;

        $doctor->name = $request->name;
        $doctor->phone = $request->phone;
        $doctor->room = $request->room;
        $doctor->speciality = $request->speciality;
        $doctor->save();
        return redirect()->back()->with('message', 'Doctor Added Successfully');
    }

    public function show_appointment()
    {
        $data = Appointment::all();
        return view("admin.show_apponitment", compact('data'));
    }
    public function approved($id)
    {
        $data = Appointment::find($id);
        $data->status = "Approved";
        $data->save();
        return redirect()->back();
    }
    public function canceled($id)
    {
        $data = Appointment::find($id);
        $data->status = "canceled";
        $data->save();
        return redirect()->back();
    }
    public function show_Doctors()
    {
        $doctors = Doctor::all();
        return view('admin.showDoctors', compact('doctors'));
    }

    public function deleteDoctor($id)
    {
        $doctor = Doctor::find($id);
        $doctor->delete();
        return redirect()->back();
    }
    public function updateDoctor($id)
    {
        $doctor = Doctor::find($id);

        return view('admin.updateDoctor', compact('doctor'));
    }
    public function edit_doctor(Request $request, $id)
    {
        // dd($request);
        $doctor = Doctor::find($id);
        // dd($doctor);
        $doctor->name = $request->name;
        $doctor->phone = $request->phone;
        $doctor->room = $request->room;
        $doctor->speciality = $request->speciality;

        $image = $request->file;
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $request->file->move("doctorimage", $imageName);
            $doctor->image = $imageName;
        }
        $doctor->save();
        return redirect()->back()->with('message', 'Doctor details updateed successfully');
    }
}
