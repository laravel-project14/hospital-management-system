<?php

namespace App\Http\Controllers;


use App\Models\Appointment;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;

class HomeController extends Controller
{
   public function redirect()
   {
      // dd(Auth::id());
      if (Auth::id()) {
        
         if (Auth::user()->userType == '0') {
            $doctor=Doctor::all();
            return view('user.home',compact('doctor'));
         } else {
            return view('admin.home');
         }
      } else {
         return redirect()->back();
      }
   }

   public function index()
   {
      if(auth::id()){
return redirect('home');
      }else{
      $doctor=Doctor::all();
      return view('user.home',compact('doctor'));
   }
   }

public function appoitment(Request $request)
{
  $data=new Appointment();
  $data->name=$request->name;
  $data->email=$request->email;
  $data->date=$request->date;
  $data->doctor=$request->doctor;
  $data->phone=$request->phone;
  $data->message=$request->message;
  $data->status="In Progress";
if(Auth::id()){

   $data->user_id=Auth::user()->id;
}
$data->save();

return redirect()->back()->with('message','Appointment Request Successful.We will contact with you soon');
}

public function my_appointment()
{
   // dd( $userId);
   if(Auth::id()){
      $userId=Auth::user()->id;
      $appoint=Appointment::where('user_id',$userId)->get();
      return view('user.my_appointment',compact('appoint'));
   }else{
      return redirect()->back();
   }
}
public function cancel_appointment($id)
{
   $data=Appointment::find($id);
   // dd($data);
   $data->delete();
   return redirect()->back();
}

}
